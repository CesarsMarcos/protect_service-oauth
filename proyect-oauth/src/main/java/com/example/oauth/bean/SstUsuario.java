package com.example.oauth.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SST_USUARIO")
public class SstUsuario implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_USUARIO")
	private Long cod;

	@Column(length = 20, unique = true, name = "USER_NAME")
	private String username;

	@Column(name = "COD_PERSONAL")
	private String codPersonal;

	@Column(length = 100, name = "DES_PASSWORD")
	private String password;

	@Column(name = "IND_VOTO")
	private String indVoto;

	@Column(name = "IND_BAJA")
	private String indBaja;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "usuario_role", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private List<SstRole> roles;

	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIndVoto() {
		return indVoto;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setIndVoto(String indVoto) {
		this.indVoto = indVoto;
	}

	public String getCodPersonal() {
		return codPersonal;
	}

	public String getIndBaja() {
		return indBaja;
	}

	public void setIndBaja(String indBaja) {
		this.indBaja = indBaja;
	}

	public List<SstRole> getRoles() {
		return roles;
	}

	public void setRoles(List<SstRole> roles) {
		this.roles = roles;
	}

	public String getCodAuxiliar() {
		return codPersonal;
	}

	public void setCodPersonal(String codPersonal) {
		this.codPersonal = codPersonal;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
