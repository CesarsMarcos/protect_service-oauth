package com.example.oauth.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.oauth.bean.SstUsuario;



@Repository
public interface UsuarioJpaRepository  extends JpaRepository<SstUsuario,Serializable>{
	
	
	public SstUsuario findByCodPersonal(String codPersonal);

	public SstUsuario findByUsername(String username);

	
}
