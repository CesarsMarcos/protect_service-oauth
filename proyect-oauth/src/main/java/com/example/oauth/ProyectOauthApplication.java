package com.example.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectOauthApplication.class, args);
	}

}
