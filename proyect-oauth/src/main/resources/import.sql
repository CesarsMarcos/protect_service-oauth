INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('CESAR22','0001','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');
INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('ARTURO05','0002','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');
INSERT INTO SST_USUARIO (USER_NAME,COD_PERSONAL,DES_PASSWORD,IND_VOTO,IND_BAJA) VALUES ('DARCY28','0003','$2a$10$GeALraV5f4bsnA7crG17yOPKey23O3ElYLod3M1PjFuavbrSijr9C','N','N');

INSERT INTO SST_ROLE (DES_ROLE) VALUES('ROLE_ADMIN');
INSERT INTO SST_ROLE (DES_ROLE) VALUES ('ROLE_USER');

INSERT INTO USUARIO_ROLE (usuario_id,role_id) VALUES (1,1);
INSERT INTO USUARIO_ROLE (usuario_id,role_id) VALUES (2,1);
INSERT INTO USUARIO_ROLE (usuario_id,role_id) VALUES (3,2);


/* Creamos algunos usuarios con sus roles 
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq',1, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
*/